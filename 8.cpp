#include <iostream>

using namespace std;

int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    if (a > b){
        if(b > c){
            cout << "Middle number is - " << b << endl;
        }else{
            cout << "Middle number is - " << c << endl;
        }
    } else if (a > c) {
        cout << "Middle number is - " << a << endl;
    } else{
        cout << "Middle number is - " << c << endl;
    }

    return 0;
}

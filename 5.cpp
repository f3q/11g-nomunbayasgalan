#include <iostream>

using namespace std;

int main()
{
    int a, b, p, u;
    cin >> a >> b;
    p = a / b;
    u = a % b;
    cout << "p = " << p << endl;
    cout << "u = " << u << endl;
    return 0;
}
